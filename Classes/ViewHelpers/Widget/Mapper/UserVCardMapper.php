<?php
/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

namespace Deichbrise\VcardViewhelper\Mapper;
use JeroenDesloovere\VCard\VCard;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;

/**
 * Class UserVCardMapper
 */
class UserVCardMapper
{
    /**
     * @var Mapper
     */
    private static $original;

    public static function mapper($object)
    {
        if(self::$original == null) {
            self::$original = new UserVCardMapper();
        }
        return self::$original->map($object);
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser $object
     * @return VCard the mapped vcard
     *
     * @throws IllegalObjectTypeException if the input object is not of type fe-user
     */
    public function map($object)
    {
        if(!($object instanceof \TYPO3\CMS\Extbase\Domain\Model\FrontendUser)) {
            throw new IllegalObjectTypeException( 'The object must be of type FrontendUser but is of type ' . get_class($object) );
        }

        $vCard = new VCard();

        $title      = $object->getTitle();
        $firstName  = $object->getFirstName();
        $middleName = $object->getMiddleName();
        $lastName   = $object->getLastName();

        // add personal data
        $vCard->addName($firstName, $lastName, $middleName, $title, '');
        $vCard->addAddress(
            null,
            null,
            $object->getAddress(),
            $object->getCity(),
            null, $object->getZip(),
            $object->getCountry()
        );

        // add work data
        $vCard->addCompany($object->getCompany());
        $vCard->addEmail($object->getEmail());
        $vCard->addPhoneNumber($object->getTelephone(), "PREF:WORK");

        $vCard->addURL($object->getWww());

        return $vCard;
    }
}