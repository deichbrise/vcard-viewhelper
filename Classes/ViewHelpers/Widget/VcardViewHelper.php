<?php
namespace Deichbrise\VcardViewhelper\ViewHelpers\Widget;

use TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetViewHelper;

/**
 * Dieser ViewHelper erzeugt aus einem FE-User Objekt eine VCard.
 *
 * <code title="required arguments (option 1)">
 * <d:widget.vcard user="{user}">Link Text</f:widget.paginate>
 *
 * {user} kann entweder ein user objekt sein oder die user id
 * </code>
 *
 * Class VcardViewHelper
 * @package Deichbrise\VcardViewhelper\ViewHelpers\Widget
 */
class VcardViewHelper extends AbstractWidgetViewHelper
{

    /**
     * @var \Deichbrise\VcardViewhelper\ViewHelpers\Widget\Controller\VCardController
     */
    protected $controller;

    /**
     * @param Controller\VCardController $controller
     */
    public function injectVCardController(\Deichbrise\VcardViewhelper\ViewHelpers\Widget\Controller\VcardController $controller)
    {
        $this->controller = $controller;
    }

    /**
     * @param \TYPO3\CMS\Extbase\Domain\Model\FrontendUser|int $user user object
     * @param array $configuration configuration object
     * @return string the rendered view
     */
    public function render($user, $configuration = array())
    {
        return $this->initiateSubRequest();
    }
}
