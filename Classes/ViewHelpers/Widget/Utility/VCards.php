<?php
namespace Deichbrise\VcardViewhelper\Utility;

use Deichbrise\VcardViewhelper\Mapper\UserVCardMapper;
use JeroenDesloovere\VCard\VCard;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException;

class VCards {

    /**
     * @param $object
     * @return VCard mapped vcard
     * @throws IllegalObjectTypeException if no mapper is registered for the input object
     */
    public static function valueOf($object) {
        switch(TRUE) {
            case $object instanceof FrontendUser:
                return UserVCardMapper::mapper($object);
                break;
            default:
                throw new IllegalObjectTypeException();
        }
    }
}