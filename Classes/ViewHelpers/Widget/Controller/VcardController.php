<?php
/*
 * This file is part of the TYPO3 CMS project.
 *
 * It is free software; you can redistribute it and/or modify it under
 * the terms of the GNU General Public License, either version 2
 * of the License, or any later version.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 * The TYPO3 project - inspiring people to share!
 */

/*
*
* author: Pascal Stammer <stammer(at)deichbrise.de>
*
*/

namespace Deichbrise\VcardViewhelper\ViewHelpers\Widget\Controller;


use Deichbrise\VcardViewhelper\Utility\VCards;
use TYPO3\CMS\Core\Utility\ArrayUtility;
use TYPO3\CMS\Extbase\Domain\Model\FrontendUser;
use TYPO3\CMS\Extbase\Domain\Repository\FrontendUserRepository;
use TYPO3\CMS\Fluid\Core\Widget\AbstractWidgetController;

class VcardController extends AbstractWidgetController
{

    protected $configuration = array(
        "userUid" => 0,
        'addQueryStringMethod' => 'POST,GET',
        'section' => ''
    );

    /**
     * @var \TYPO3\CMS\Extbase\Domain\Model\FrontendUser | int
     */
    protected $user;

    /**
     * @var FrontendUserRepository
     */
    protected $userRepository;

    public function injectUserRepository(FrontendUserRepository $userRepository) {
        $this->userRepository = $userRepository;
    }

    /**
     * @return void
     */
    public function initializeAction()
    {
        ArrayUtility::mergeRecursiveWithOverrule($this->configuration, $this->widgetConfiguration['configuration'], false);
        $this->user = $this->widgetConfiguration['user'];
    }


    /**
     * Index Action
     */
    public function indexAction() {
        $this->view->assign('configuration', $this->configuration);
    }


    /**
     * Download the V-Card
     *
     * @throws \TYPO3\CMS\Extbase\Persistence\Exception\IllegalObjectTypeException
     */
    public function downloadAction() {
        // We ignore NULL check because of previous requirement check
        if(!($this->user instanceof FrontendUser)) {
            $userId =$this->user;
            $this->user = $this->userRepository->findByUid($userId);
            if($this->user == NULL) {
                throw new \InvalidArgumentException( "No user found for id " . $userId );
            }
        }

        VCards::valueOf($this->user)->download();
    }
}