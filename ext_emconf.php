<?php


$EM_CONF[$_EXTKEY] = array(
	'title' => 'VCard Viewhelper',
	'description' => 'VCard ViewHelper',
	'category' => 'plugin',
	'author' => 'Pascal Stammer',
	'author_email' => 'stammer@deichbrise.de',
	'state' => 'alpha',
	'internal' => '',
	'uploadfolder' => '1',
	'createDirs' => '',
	'clearCacheOnLoad' => 0,
	'version' => '7.6',
	'constraints' => array(
		'depends' => array(
			'typo3' => '7.6.0-7.6.99',
		),
		'conflicts' => array(
		),
		'suggests' => array(
		),
	),
);